class AddInfoToTraktors < ActiveRecord::Migration
  def change
    add_column :traktors, :image, :string
    add_column :traktors, :year, :integer
    add_column :traktors, :usage, :integer
    add_column :traktors, :color, :string
    add_column :traktors, :description, :text
  end
end
