class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :cart_id
      t.integer :user_id
      t.string :delivery_type
      t.string :payment_type
      t.string :phone
      t.string :address
      t.boolean :finished

      t.timestamps
    end
  end
end
