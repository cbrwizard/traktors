class ShuffleOrders < ActiveRecord::Migration
  def change
    add_column :carts, :amount, :integer
    add_column :carts, :order_id, :integer
    remove_column :orders, :cart_id
  end
end
