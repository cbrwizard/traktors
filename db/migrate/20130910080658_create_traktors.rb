class CreateTraktors < ActiveRecord::Migration
  def change
    create_table :traktors do |t|
      t.string :name
      t.integer :cost

      t.timestamps
    end
  end
end
