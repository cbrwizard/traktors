class AddDefaults < ActiveRecord::Migration
  def change
    change_column :carts, :user_id, :integer, :null => false
    change_column :carts, :traktor_id, :integer, :null => false
    change_column :carts, :amount, :integer, :default => 1

    change_column :images, :traktor_id, :integer, :null => false
    change_column :images, :url, :string, :null => false

    change_column :orders, :delivery_type, :string, :null => false
    change_column :orders, :payment_type, :string, :null => false
    change_column :orders, :phone, :string, :null => false
    change_column :orders, :address, :string, :null => false
    change_column :orders, :finished, :boolean, :default => false

    change_column :traktors, :name, :string, :null => false
    change_column :traktors, :cost, :integer, :null => false
    change_column :traktors, :year, :integer, :null => false
    change_column :traktors, :usage, :integer, :null => false
    change_column :traktors, :color, :string, :null => false
    change_column :traktors, :description, :string, :null => false

  end
end