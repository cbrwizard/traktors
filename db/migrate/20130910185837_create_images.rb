class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.integer :traktor_id
      t.string :url
      t.timestamps
    end
    remove_column :traktors, :image
  end
end
