# encoding: utf-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Traktor.create([
  {name: 'Трактор Эпический', cost: 3000000, year: 2013, usage: 500, color: "Красный", description: "Трактор-смерть. Сделает за вас все дела и убьет ваших врагов."},
  {name: 'Трактор YTO Belarus', cost: 50000, year: 2010, usage: 0, color: "Красный", description: "Посеет, пожнет, положит, подавит, порадуется, поедет."},
  {name: 'Трактор Желток', cost: 1234562, year: 2012, usage: 0, color: "Желтый", description: "Самый продаваемый товар на рынке дальнего востока."},
  {name: 'Трактор Убийца Дорог', cost: 666666, year: 2010, usage: 1313, color: "Синий", description: "Уничтожит дороги ваших соседей. Отрежет деревню от цивилизации. Повысит затраты на дорожные работы."},
  {name: 'Трактор Беларусь 8.21', cost: 5676, year: 1999, usage: 10500, color: "Синий", description: "Травит байки. Матерится. Иногда теряет по ходу детали."},
  {name: 'Трактор Петра', cost: 134132, year:2008, usage: 0, color: "Красный", description: "Специальный трактор для путешествий в другие страны."}])
Image.create([
  {url: "traktors/1.jpg", traktor_id: 1},
  {url: "traktors/2/1.jpg", traktor_id: 2},
  {url: "traktors/2/2.jpg", traktor_id: 2},
  {url: "traktors/2/3.jpg", traktor_id: 2},
  {url: "traktors/3/1.jpg", traktor_id: 3},
  {url: "traktors/4/1.jpg", traktor_id: 4},
  {url: "traktors/5/1.jpg", traktor_id: 5},
  {url: "traktors/5/2.jpeg", traktor_id: 5},
  {url: "traktors/5/3.jpg", traktor_id: 5},
  {url: "traktors/6/1.jpg", traktor_id: 6},
  {url: "traktors/6/2.jpeg", traktor_id: 6},])
User.create([
  {admin: true, email: "admin@mail.ru", password: "qwerty"},
  {moderator: true, email: "moderator@mail.ru", password: "qwerty"},
  {email: "user@mail.ru", password: "qwerty"}])
