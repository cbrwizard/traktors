class Order < ActiveRecord::Base
  belongs_to :user
  has_many :carts
  has_many :traktors, :through => :cart
  has_one :user, :through => :cart

  attr_accessible :address, :delivery_type, :finished, :payment_type, :phone
end
