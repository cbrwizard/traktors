class Image < ActiveRecord::Base
  attr_accessible :traktor_id, :url

  belongs_to :traktor
end
