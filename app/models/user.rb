class User < ActiveRecord::Base
  has_many :carts
  has_many :orders, :through => :carts


  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :admin, :moderator
  # attr_accessible :title, :body

  validates_format_of :email,:with => Devise.email_regexp
end
