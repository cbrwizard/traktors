class Cart < ActiveRecord::Base
  belongs_to :user
  belongs_to :traktor
  belongs_to :order

  attr_accessible :traktor_id, :user_id, :order_id, :amount

end
