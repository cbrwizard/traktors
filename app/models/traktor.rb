class Traktor < ActiveRecord::Base
  has_many :images
  has_many :carts
  has_many :orders, :through => :carts

  attr_accessible :cost, :name, :description, :year, :usage, :color

end
