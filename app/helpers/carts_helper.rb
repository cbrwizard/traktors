module CartsHelper
  def calculate_carts
    if user_signed_in?
      @user_carts = current_user.carts.where(:order_id => nil)
    end
  end
end
