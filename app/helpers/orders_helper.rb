module OrdersHelper
  def calculate_orders
    if user_signed_in?
      @user_orders = current_user.orders.where(:finished => false).uniq
    end
  end
end
