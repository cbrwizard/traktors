class ApplicationController < ActionController::Base
  include CartsHelper
  include OrdersHelper
  protect_from_forgery
  before_filter :calculate_carts, :calculate_orders

  def require_admin
    unless current_user.try(:admin?) || current_user.try(:moderator)
      redirect_to root_url
    end
  end
end
