# encoding: utf-8
class TraktorsController < ApplicationController

  before_filter :require_admin, :except => [:buy, :index, :show, :search]

  def buy
    id = params[:id]
    traktor = Traktor.find(id)
    if user_signed_in? && traktor
      current_user.carts.create(traktor_id: id)
    end
    redirect_to root_url
    flash[:success] = "Вы добавили #{traktor.name} в корзину"
  end

  def search
    @traktors = []
    Traktor.find_each do |traktor|
      if traktor.name.mb_chars.downcase.include? params[:name].mb_chars.downcase
        @traktors << traktor
      end
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @traktors }
    end
  end

  # GET /traktors
  # GET /traktors.json
  def index
    @traktors = Traktor.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @traktors }
    end
  end

  # GET /traktors/1
  # GET /traktors/1.json
  def show
    @traktor = Traktor.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @traktor }
    end
  end

  # GET /traktors/new
  # GET /traktors/new.json
  def new
    @traktor = Traktor.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @traktor }
    end
  end

  # GET /traktors/1/edit
  def edit
    @traktor = Traktor.find(params[:id])
  end

  # POST /traktors
  # POST /traktors.json
  def create
    @traktor = Traktor.new(params[:traktor])

    respond_to do |format|
      if @traktor.save
        format.html { redirect_to @traktor, notice: 'Traktor was successfully created.' }
        format.json { render json: @traktor, status: :created, location: @traktor }
      else
        format.html { render action: "new" }
        format.json { render json: @traktor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /traktors/1
  # PUT /traktors/1.json
  def update
    @traktor = Traktor.find(params[:id])

    respond_to do |format|
      if @traktor.update_attributes(params[:traktor])
        format.html { redirect_to @traktor, notice: 'Traktor was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @traktor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /traktors/1
  # DELETE /traktors/1.json
  def destroy
    @traktor = Traktor.find(params[:id])
    @traktor.destroy

    respond_to do |format|
      format.html { redirect_to traktors_url }
      format.json { head :no_content }
    end
  end
end
