notyHide = -> #чтобы убирались noty при наведении
  $(".i-am-new").hover ->
    $(this).slideUp 400, ->
      $.noty.closeAll()
  $.noty.clearQueue()

if $(".flashMessage").length > 0 #отображает оповещения при регистрации, создании мнения и тд
  notif = noty(
    text: $(".flashMessage p").text()
    type: $(".flashMessage p").attr("class")
    layout: "topCenter"
    timeout: 3500
  )
  notyHide()